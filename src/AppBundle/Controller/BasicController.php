<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BasicController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $places = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Place')
            ->findAll();

        $dishesByPlaces = [];

        foreach ($places as $place) {
            $dishes = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Dish')
                ->getMostPopularDishesByPlace($place, 2);
            $dishesByPlaces[$place->getName()] = $dishes;
        }

        return $this->render('@App/Basic/index.html.twig', array(
            'places' => $places,
            'dishes_by_places' => $dishesByPlaces
        ));
    }

    /**
     * @Route("/place/{id}", name="place_info")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function placeInfoAction($id)
    {
        $place = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Place')
            ->findOneBy(['id' => $id]);
        return $this->render('@App/Basic/place_info.html.twig', array(
            'place' => $place
        ));
    }

}
