<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Dish;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDishData extends Fixture implements DependentFixtureInterface
{
    private static $dishes = [];

    public function load(ObjectManager $manager)
    {
        $namesOfDishes = [
            "Ролы Филадельфия",
            "Темпура",
            "Сэндвич",
            "Мисо Суп",
            "Якотри",
            "Стейк",
            "Панини",
            "Рулет с курицей",
            "Бургер",
            "Бублик"
        ];
        foreach ($namesOfDishes as $key => $oneOfDish) {
            $dishKey = "dish{$key}";
            $dish = new Dish();
            $dish
                ->setName($oneOfDish)
                ->setPrice($this->getRandomPrice())
                ->setImage($dishKey . '.png');

            if ($key < 5) {
                $dish->setPlace($this->getReference(LoadPlaceData::PLACE_ONE));
            } else {
                $dish->setPlace($this->getReference(LoadPlaceData::PLACE_TWO));
            }
            $manager->persist($dish);

            $this->addReference($dishKey, $dish);
            self::$dishes[] = $dishKey;
        }

        $manager->flush();

    }

    function getDependencies()
    {
        return array(
            LoadPlaceData::class
        );
    }

    public function getRandomPrice()
    {
        return rand(100, 300);
    }

    public static function getDishes()
    {
        return self::$dishes;
    }
}