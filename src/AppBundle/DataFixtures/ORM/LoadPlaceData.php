<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Place;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPlaceData extends Fixture
{
    public const PLACE_ONE = 'SushiRoom';
    public const PLACE_TWO = 'Бублик';

    public function load(ObjectManager $manager)
    {
        $place1 = new Place();
        $place1
            ->setName('SushiRoom')
            ->setDescription('СушиРум — кафе современной японской кухни на бульваре Эркиндик. В основе меню — разнообразные суши, приготовленные на местный манер')
            ->setImage('sushiroom.jpeg');

        $manager->persist($place1);

        $place2 = new Place();
        $place2
            ->setName('Бублик')
            ->setDescription('Бублик — небольшое уютное заведение, открывшееся в центре Бишкека в конце февраля 2015. ')
            ->setImage('bublik.jpg');

        $manager->persist($place2);
        $manager->flush();

        $this->addReference(self::PLACE_ONE, $place1);
        $this->addReference(self::PLACE_TWO, $place2);
    }
}
