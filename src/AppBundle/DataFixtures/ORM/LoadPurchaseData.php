<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Purchase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class LoadPurchaseData extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $arrayDishes = LoadDishData::getDishes();

        for ($i = 1000; $i > 1; $i--) {
            $purchase = new Purchase();
            $date = new \DateTime(date("Y-m-d H:i:s", time() - floor($i * 43000)));
            $purchase
                ->setDate($date)
                ->setDish($this->getReference($arrayDishes[rand(0, 9)]));
            $manager->persist($purchase);
        }

        $manager->flush();
    }

    function getDependencies()
    {
        return array(
            LoadDishData::class
        );
    }

}